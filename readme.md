# Temperature logger on Raspberry Pie Zero

There are 2 versions of the storage:
- storage to a local text file ("t_log_file.py")
- storage to a remote database ("t_log_db.py")


### The database storage:
Retrieve temperatures from an array of DS18B20 sensors and store it in a local SQLite database.
After a few cycles send everything to a remote database for permanent storage.
The database has to be prepared before this script is run.
Ther is also a "database_credentials.py" file which holds login information (git stores only an empty template file).
