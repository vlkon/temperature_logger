#!/usr/bin/env python3

# -----------------------------------------------------------------------------
import time
from datetime import datetime
import subprocess
import signal

import sqlite3
import mysql.connector as mariadb   # mysql connector works good enough with mariadb for this application needs
import database_credentials as dc   # custom file - template should be part of git repo

# -----------------------------------------------------------------------------
# SQLite database for temporary storage - creates it if it doesn't exist
DB_TEMPORARY    = '/tmp/temperature_storage.db'

# MariaDB for permanent storage - has to be prepared before this script is run
DB_USER         = dc.username
DB_HOST         = dc.host
DB_PASSWORD     = dc.password
DB_DATABASE     = dc.database

SENSORS = ["/sys/bus/w1/devices/28-00000b54f82c/",      # T_in
           "/sys/bus/w1/devices/28-00000b55c113/",      # T_out
           "/sys/bus/w1/devices/28-00000b547097/",      # T_2
           "/sys/bus/w1/devices/28-00000b5582ba/",      # T_3
           "/sys/bus/w1/devices/28-00000b55fcea/",      # T_4
           "/sys/bus/w1/devices/28-00000b540833/",      # T_5
           "/sys/bus/w1/devices/28-00000b54a4db/",      # T_6
           "/sys/bus/w1/devices/28-00000b560c56/"       # T_7
          ]

UPLOAD_INTERVAL = 199   # number of main loop cycles to upload data from temporary to permanent database (N+1 entries)
LOOP_DELAY      = 0.5   # prevent too fast looping - it may not be necessary since each sensor DS18B20 takes 0.75s to read its value


# -----------------------------------------------------------------------------
# check important external resources
if DB_HOST == '':
    print('Database credentials are missing. Please fill correctly "database_credentials.py" file.')
    exit(1)


# -----------------------------------------------------------------------------
# provide a way to break infinite main loop
INF_LOOP = True
def break_loop(sig, frame):
    global INF_LOOP
    INF_LOOP = False

signal.signal(signal.SIGINT, break_loop)
signal.signal(signal.SIGTERM, break_loop)

# -----------------------------------------------------------------------------
# ping database before upload - check stability of the connection
def ping(host):
    tries = 3   # 1 or more
    command = ['ping', '-c', str(tries), '-w', str(tries + 1), host]
    ret_code = subprocess.run(command,
                              stdout=subprocess.DEVNULL,
                              stderr=subprocess.DEVNULL
                             ).returncode
    return ret_code == 0    # True if ping passed all packets


def tmp_database_commit(db_file, sql_command, values):
    # template for communicating with sqlite database
    con = sqlite3.connect(db_file)
    try:
        with con:
            cur = con.cursor()
            cur.execute(sql_command, values)
            con.commit()
    except sqlite3.Error as e:
        print(e)


def build_tmp_database(db_file):
    # build temporary sqlite database
    sql_command = """CREATE TABLE IF NOT EXISTS temperatures (
                        --timestamp DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL PRIMARY KEY,
                        timestamp DATETIME DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME')) NOT NULL PRIMARY KEY,
                        T_cpu FLOAT,
                        T_in FLOAT,
                        T_out FLOAT,
                        T_2 FLOAT,
                        T_3 FLOAT,
                        T_4 FLOAT,
                        T_5 FLOAT,
                        T_6 FLOAT,
                        T_7 FLOAT
                        );"""
    tmp_database_commit(db_file, sql_command, '')


def insert_to_tmp_database(db_file, values):
    # store data in the temporary database
    sql_command = """INSERT OR IGNORE INTO temperatures (T_cpu, T_in, T_out, T_2, T_3, T_4, T_5, T_6, T_7)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);"""
    tmp_database_commit(db_file, sql_command, values)


def clear_tmp_database(db_file):
    # delete all data from temporary database
    sql_command = """DELETE FROM temperatures;"""
    tmp_database_commit(db_file, sql_command, '')


def fetch_from_tmp_database(db_file):
    # get all data from the temporary database into a list of tuples
    sql_command = """SELECT * FROM temperatures;"""
    con = sqlite3.connect(db_file)
    try:
        with con:
            cur = con.cursor()
            cur.execute(sql_command)
            values = cur.fetchall()

        err = 0     # no problem during data read
    except sqlite3.Error as e:
        err = 1     # some problem
        print(e)

    return (err, values)


def get_cpu_temp():
    result = subprocess.run(['/opt/vc/bin/vcgencmd', 'measure_temp'], capture_output=True, text=True)
    temperature = result.stdout
    if temperature != '':
        t_cpu = float(temperature.split('=')[1].split('\'')[0])
    else:
        t_cpu = None
    return t_cpu


def get_external_temperatures():
    # create a list of temperatures based on SENSORS list
    t_list = []
    for sensor in SENSORS:
        with open(sensor + 'temperature', "r") as f:
            t = f.read()    # raw temperature value - should be in milli C
            if t != '':     # read failed for some reason - for example a SIGINT was received
                temperature = float(int(t) / 1000)      # return values as float number
                #t_list.append('{:.1f}'.format(t))       # return values as string with 1 decimal place
            else:
                temperature = None

            t_list.append(temperature)
    return t_list


def upload_data(database, host, user, password, values):
    err = 0      # 0==OK, >0 error during upoad
    sql_upload = """INSERT INTO temperatures(timestamp, T_cpu, T_in, T_out, T_2, T_3, T_4, T_5, T_6, T_7)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                            ON DUPLICATE KEY UPDATE timestamp=timestamp;"""     # if the entry is already there - do nothing
    try:
        #connection = mariadb.connect(host=DB_HOST, user=DB_USER, password=DB_PASSWORD, database=DB_DATABASE)
        connection = mariadb.connect(host=host, user=user, password=password, database=database)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.executemany(sql_upload, values)
            connection.commit()
        else:
            err = 2
    except mariadb.Error as e:
        err = 1
        print('MariaDB error: ', e)
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
    return err


def upload_tmp_database(skip_ping_check=False):
    # wrapper for uploading tmp to permanent database
    if skip_ping_check or ping(DB_HOST):    # first check if the permanent database is available
        (error, values) = fetch_from_tmp_database(DB_TEMPORARY)
        if error == 0:
            if upload_data(DB_DATABASE, DB_HOST, DB_USER, DB_PASSWORD, values) == 0:
                clear_tmp_database(DB_TEMPORARY)
        return error
    else:
        return -1   # database unreachable


def main():
    build_tmp_database(DB_TEMPORARY)

    cycle_counter = 0
    while INF_LOOP:
        temperatures = []

        # get all available data to a storage list
        temperatures.append(get_cpu_temp())
        temperatures += get_external_temperatures()

        # dump data to the local temporary sqlite database
        insert_to_tmp_database(DB_TEMPORARY, temperatures)

        #if certain ammount of cycles - dump local database to permanent remote database
        if cycle_counter >= UPLOAD_INTERVAL:
            cycle_counter = 0       # reset counter not mater if the upload was success or not - it can be uploaded in next cycle without too much problem
            upload_tmp_database()

        else:
            cycle_counter += 1

        # some delay that prevents too fast looping
        time.sleep(LOOP_DELAY)

    # try to upload everything remaining in the database before the script terminates
    upload_tmp_database(True)

if(__name__ == '__main__'):
    main()
