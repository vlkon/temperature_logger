#!/usr/bin/env python3

import time
from datetime import datetime
import subprocess
#import signal
#from functools import partial

SENSORS = [["t_in",     "/sys/bus/w1/devices/28-00000b54f82c/"],
           ["t_out",    "/sys/bus/w1/devices/28-00000b55c113/"],
           ["t_2",      "/sys/bus/w1/devices/28-00000b547097/"],
           ["t_3",      "/sys/bus/w1/devices/28-00000b5582ba/"],
           ["t_4",      "/sys/bus/w1/devices/28-00000b55fcea/"],
           ["t_5",      "/sys/bus/w1/devices/28-00000b540833/"],
           ["t_6",      "/sys/bus/w1/devices/28-00000b54a4db/"],
           ["t_7",      "/sys/bus/w1/devices/28-00000b560c56/"]
          ]

SEPARATOR   = ';'
NEWLINE     = '\n'
FILE_TMP    = '/tmp/temperature.log'
FILE_PERM   = '~/temperature.log'

T_CPU = True
T_EXT = True

def main():

    # build file header
    with open(FILE_TMP, "w") as f:
        line = 'timestamp'

        if(T_CPU == True):
            line += SEPARATOR + 't_cpu'

        if(T_EXT == True):
            for sensor in SENSORS:
                line += SEPARATOR + sensor[0]

        line += NEWLINE
        f.write(line)

    # continuously get temperature data
    while True:
        time.sleep(0.1)     # safety precaution to prevent high CPU load
        line = ''

        # get time
        timeObj = datetime.now()
        timestamp = timeObj.strftime("%Y-%m-%dT%H:%M:%S")
        line += timestamp

        # get GPU temperature
        if(T_CPU == True):
            result = subprocess.run(['/opt/vc/bin/vcgencmd', 'measure_temp'], capture_output=True, text=True)
            temperature = result.stdout
            line += ';'
            line += temperature.split('=')[1].split('\'')[0]

        # get temperatures from external sensors
        if(T_EXT == True):
            for sensor in SENSORS:
                with open(sensor[1] + 'temperature', "r") as f:
                    temperature = int(f.read()) / 1000
                    line += SEPARATOR
                    line += '{:.1f}'.format(temperature)

        # add newline at the end of the line
        line += '\n'


        with open(FILE_TMP, "a") as f:
            f.write(line)


if(__name__ == '__main__'):
    main()
