#!/bin/bash

name="temperature_logger"
user="t_logger"
launcher="t_log_db.py"
run_dir="/opt/${name}"

#-----------------------------------------------------
if [[ "$EUID" -ne 0 ]]; then
    echo "Please run as root"
    exit
fi

#-----------------------------------------------------
if [[ "$1" == "install" ]]; then
    # setup a dedicated user that will run the script
    useradd -r -U -s '/usr/bin/nologin' -G 'i2c,video' "${user}"
    echo "user ${user} created"

    # copy files to proper place
    mkdir "${run_dir}"
    cp "./${launcher}" "${run_dir}/${launcher}"
    cp "./database_credentials.py" "${run_dir}/database_credentials.py"
    chown -R "${user}":"${user}" "${run_dir}"
    echo "all files prepared"

    # setup systemd service
    cp "./${name}.service" "/etc/systemd/system/${name}.service"
    cp "./${name}_restart.timer" "/etc/systemd/system/${name}_restart.timer"
    cp "./${name}_restart.service" "/etc/systemd/system/${name}_restart.service"
    sed -i -e "/ExecStart/s,/opt/temperature_logger/t_log_db.py,${run_dir}/${launcher}," "/etc/systemd/system/${name}.service"
    sed -i -e "/User/s,t_logger,${user}," "/etc/systemd/system/${name}.service"
    sed -i -e "/Group/s,t_logger,${user}," "/etc/systemd/system/${name}.service"
    echo "${name} service file prepared"

    # start the service
    systemctl enable "${name}.service"
    systemctl enable "${name}_restart.timer"
    systemctl start "${name}.service"
    systemctl start "${name}_restart.timer"
    echo "${name} service installed and started"

    echo "done"


elif [[ "$1" == "uninstall" ]]; then
    # stop service
    systemctl stop "${name}.service"
    systemctl stop "${name}_restart.timer"
    systemctl disable "${name}.service"
    systemctl disable "${name}_restart.timer"
    echo "${name} service stopped and uninstalled"

    # remove all files
    rm -rf "${run_dir}"
    rm "/etc/systemd/system/${name}.service"
    rm "/etc/systemd/system/${name}_restart.timer"
    rm "/etc/systemd/system/${name}_restart.service"
    echo "files at ${run_dir} deleted"

    # remove user
    userdel "${user}"
    echo "user ${user} deleted"

    echo "done"


else
    echo "'install' or 'uninstall' parameter has to be specified"
    exit

fi
